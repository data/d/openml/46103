# OpenML dataset: FitBit_HeartRate

https://www.openml.org/d/46103

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The 'heartrate_seconds_merged.csv' dataset is a comprehensive compilation of heart rate measurements collected over varying days and times, intended to provide insights into heart rate variability across different individuals and time periods. The dataset is structured into three primary columns: 'Id', 'Time', and 'Value'. 

Attribute Description:
- 'Id': This column contains unique numerical identifiers (e.g., 4020332650, 6962181067) for each participant in the study, allowing for the anonymized tracking of heart rate data across multiple entries.
- 'Time': Each entry in this column records the specific date and time when the heart rate measurement was taken, formatted as 'Month/Day/Year Hours:Minutes:Seconds AM/PM' (e.g., '4/5/2016 8:00:15 AM'). This attribute is crucial for understanding the temporal context of each heart rate measurement.
- 'Value': Represents the heart rate of an individual at the recorded time, measured in beats per minute (BPM). Sample values in this dataset range from 55 to 92 BPM, indicating the variability of heart rate measurements among different times and individuals.

Use Case:
This dataset can serve multiple analytical and research purposes, such as studying the effects of various factors (e.g., time of day, physical activity, stress) on heart rate variability. It can also be utilized in developing heart rate monitoring applications, conducting temporal heart rate variability studies, and enhancing personalized health and fitness recommendations. Researchers and healthcare professionals could leverage this dataset to understand patterns in heart rate data, thereby contributing to broader studies in cardiology, health informatics, and personalized medicine.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46103) of an [OpenML dataset](https://www.openml.org/d/46103). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46103/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46103/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46103/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

